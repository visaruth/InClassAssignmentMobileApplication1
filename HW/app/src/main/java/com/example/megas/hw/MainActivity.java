package com.example.megas.hw;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    String filePath;
    String fileName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        final ImageButton btn1 = (ImageButton) findViewById(R.id.camera_btn);
        btn1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "pkl_" + timeStamp + ".jpg";
                File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageFileName);
                Uri fileUri = Uri.fromFile(f);
                filePath = fileUri.toString();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, 0);
            }
        });

        final ImageButton btn3 = (ImageButton)
                findViewById(R.id.gallery_btn);
        btn3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });

        final Button btnDownload = (Button)findViewById(R.id.button3);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int count;
                String from = "http://www.lstpch.com/android/pkl_20151030_162439.jpg";
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/";
                //String path = "/storage/emulated/0/";
                try {
                    URL url = new URL(from);
                    URLConnection con = url.openConnection();
                    con.connect();
                    InputStream input = new BufferedInputStream(url.openStream());
                    String fileName = from.substring(from.lastIndexOf('/')+1, from.length());
                    OutputStream output = new FileOutputStream(path+fileName); // save to file path
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                    Toast.makeText(MainActivity.this,"File has been downloaded. " ,Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    Toast.makeText(MainActivity.this,"Download Error!" ,Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    public void upload(View view){
        if(SaveData()){
            Toast.makeText(this, "uploaded", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean SaveData(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("error");
        dialog.setIcon(android.R.drawable.ic_menu_info_details);
        dialog.setPositiveButton("Close", null);
        if (filePath != null) {
            String strUrlServer = "http://www.lstpch.com/android/uploadFile.php";
            filePath = filePath.replace("file://", "");
            String resultServer = uploadFiletoServer(filePath, strUrlServer);
            String strStatusID = "0";
            String strError = "Unknow Status!";
            try {
                JSONObject c = new JSONObject(resultServer);
                strStatusID = c.getString("StatusID");
                strError = c.getString("Error");
            } catch (JSONException e) {
                Toast.makeText(MainActivity.this,"Upload Error!" ,Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            if (strStatusID.equals("0")) {
                dialog.setMessage(strError);
                dialog.show();
                return false;
            } else {
                dialog.setTitle("Upload success");
                dialog.setMessage("success");
                dialog.show();
                filePath = null;
                return true;
            }
        }else {
            Toast.makeText(MainActivity.this,"File not found!" ,Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public String uploadFiletoServer(String strFilePath, String strUrlServer) {
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        int resCode = 0;
        String resMessage = "";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";
        try {
            File file = new File(strFilePath);
            if(!file.exists())
            {
                return "{\"StatusID\":\"0\",\"Error\":\"Please check file path\"}";
            }
            FileInputStream fileInputStream = new FileInputStream(new File(strFilePath));
            URL url = new URL(strUrlServer);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            DataOutputStream outputStream = new DataOutputStream(conn.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"filUpload\";filename=\"" + strFilePath + "\"" + lineEnd);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            resCode = conn.getResponseCode();
            if(resCode == HttpURLConnection.HTTP_OK){
                InputStream is = conn.getInputStream();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                int read = 0;
                while ((read = is.read()) != -1) {
                    bos.write(read);
                }
                byte[] result = bos.toByteArray();
                bos.close();
                resMessage = new String(result);
            }

            Log.d("resCode=",Integer.toString(resCode));
            Log.d("resMessage=",resMessage);

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            return resMessage;

        } catch (Exception ex) {
            return null;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            Bitmap bmpPic = BitmapFactory.decodeFile(filePath.replace("file://", ""));
            FileOutputStream bmpFile = new FileOutputStream(filePath.replace("file://", ""));
            bmpPic = Bitmap.createScaledBitmap(bmpPic, 600, 400, true);
            Matrix mat = new Matrix();
            mat.postRotate(90);
            bmpPic = Bitmap.createBitmap(bmpPic, 0, 0, bmpPic.getWidth(), bmpPic.getHeight(), mat, true);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, 10, bmpFile);
            bmpFile.flush();
            bmpFile.close();
            ImageButton img = (ImageButton) findViewById(R.id.camera_btn);
            img.setImageBitmap(bmpPic);
        } catch (Exception e) {
            Log.e("Log", "Error on saving file");
        }

        if (requestCode== 2 && resultCode == Activity.RESULT_OK) {
            try {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                filePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmpPic = BitmapFactory.decodeFile(filePath);
                ImageButton img = (ImageButton) findViewById(R.id.gallery_btn);
                img.setImageBitmap(bmpPic);
                TextView txt = (TextView) findViewById(R.id.textView);
                txt.setText("Gallery");
            } catch (Exception e) {
                Log.e("Log", "Error from Gallery Activity");
            }
        }
    }
}
